��          �      L      �     �     �     �     �  .     	   1     ;     G     ]     r     �     �     �     �     �     �     �       �       �               3  9   D     ~  
   �  %   �     �  	   �     �     �     �       &   3     Z  !   _     �                                	                                                     
               Choose directory Create a thumbnail Custom Area Delay in seconds File extension was changed.\nPlease try again. File type Full Screen Keep this screenshot? Multiple screenshots Name of image Percentage (1-100) Region to capture Show save dialog Take another shot Thumbnail for webpages Window Your screenshot has been saved. antiXscreenshot Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-11-27 21:19+0200
Last-Translator: Arnold Marko <arnold.marko@gmail.com>
Language-Team: Slovenian (http://www.transifex.com/anticapitalista/antix-development/language/sl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sl
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
X-Generator: Poedit 2.3
 Izberi imenik Ustvari predogledno sličico Določitev območje Zamik v sekundah Končnica datoteke je bila spremenjena.\nPoskusite znova. Vrsta datoteke Cel zaslon Ali naj obdržim ta posnetek zaslona? Več posnetkov zaslona Ime slike Odstoki (1-100) Območje zajema Prikaži dialog za shranjevanje Naredi nov posnetek Predogledne sličice za spletne strani Okno Posnetek zaslona je bil shranjen. antiXzaslon 