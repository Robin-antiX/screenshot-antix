��          �      L      �     �     �     �     �  .     	   1     ;     G     ]     r     �     �     �     �     �     �     �       �       �          $     4  :   L     �     �     �     �     �     �     �  (   	     2  $   M     r  &   �     �                                	                                                     
               Choose directory Create a thumbnail Custom Area Delay in seconds File extension was changed.\nPlease try again. File type Full Screen Keep this screenshot? Multiple screenshots Name of image Percentage (1-100) Region to capture Show save dialog Take another shot Thumbnail for webpages Window Your screenshot has been saved. antiXscreenshot Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-15 17:01+0200
Last-Translator: joyinko <joyinko@azet.sk>
Language-Team: Slovak (http://www.transifex.com/anticapitalista/antix-development/language/sk/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sk
Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n == 1 ? 0 : n % 1 == 0 && n >= 2 && n <= 4 ? 1 : n % 1 != 0 ? 2: 3);
X-Generator: Poedit 2.2.1
 Zvoliť adresár Vytvoriť miniatúru Vybrať oblasť Omeškanie v sekundách Prípona súboru bola zmenená.\nSkúste to znovu prosím. Typ súboru Celá obrazovka Uložiť snímku? Viacnásobná snímka obrazovky Názov obrázku Percentá (1-100) Zachytiť oblasť Zobraziť dialógové okno pre uloženie Vytvoriť ďalšiu snímku Miniatúra pre internetové stránky Aktívne okno Vaša snímka obrazovky bola uložená antiXscreenshot 