��          �      L      �     �     �     �     �  .     	   1     ;     G     ]     r     �     �     �     �     �     �     �       �       �     �     �       ,        G     U     a     {     �     �     �     �     �     �       '        =                                	                                                     
               Choose directory Create a thumbnail Custom Area Delay in seconds File extension was changed.\nPlease try again. File type Full Screen Keep this screenshot? Multiple screenshots Name of image Percentage (1-100) Region to capture Show save dialog Take another shot Thumbnail for webpages Window Your screenshot has been saved. antiXscreenshot Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-15 17:00+0200
Last-Translator: Sveinn í Felli <sv1@fellsnet.is>
Language-Team: Icelandic (http://www.transifex.com/anticapitalista/antix-development/language/is/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: is
Plural-Forms: nplurals=2; plural=(n % 10 != 1 || n % 100 == 11);
X-Generator: Poedit 2.2.1
 Veldu möppu Búa til smámynd Sérsniðið svæði Töf í sekúndum Skráarendingu var breytt.\nPrófaðu aftur. Skráartegund Fylla skjá Halda þessari skjámynd? Margar skjámyndir Nafn á mynd Prósentuhlutfall (1-100) Svæði sem á að grípa Birta vistunarglugga Taka aðra mynd Smámynd fyrir vefsíður Gluggi Skjámyndin þín hefur verið vistuð. antiXskjámynd 