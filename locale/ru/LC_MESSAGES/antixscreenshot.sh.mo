��          �      L      �     �     �     �     �  .     	   1     ;     G     ]     r     �     �     �     �     �     �     �       ?       V  '   t  B   �  $   �  _        d     v  -   �  '   �     �  /     $   2  6   W  (   �  5   �     �  0   �     '                                	                                                     
               Choose directory Create a thumbnail Custom Area Delay in seconds File extension was changed.\nPlease try again. File type Full Screen Keep this screenshot? Multiple screenshots Name of image Percentage (1-100) Region to capture Show save dialog Take another shot Thumbnail for webpages Window Your screenshot has been saved. antiXscreenshot Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-15 17:01+0200
Last-Translator: Вячеслав Волошин <vol_vel@mail.ru>
Language-Team: Russian (http://www.transifex.com/anticapitalista/antix-development/language/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
X-Generator: Poedit 2.2.1
 Выбрать каталог Создать предпросмотр Определенная пользователем область Задержка в секундах Расширение файла было изменено.\nПопробуйте еще раз. Тип файла Полный экран Сохранить этот скриншот? Несколько скриншотов Имя изображения Процентное отношение (1-100) Область для захвата Показывать диалог сохранения Сделать другой снимок Предпросмотр для веб-страниц Окно Ваш скриншот был сохранен. antiX Снимок экрана 