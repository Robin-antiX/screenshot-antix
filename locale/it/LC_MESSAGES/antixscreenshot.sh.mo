��          �      L      �     �     �     �     �  .     	   1     ;     G     ]     r     �     �     �     �     �     �     �       �       �     �     �       >        V     c     r     �     �     �     �      �     �          0  "   9     \                                	                                                     
               Choose directory Create a thumbnail Custom Area Delay in seconds File extension was changed.\nPlease try again. File type Full Screen Keep this screenshot? Multiple screenshots Name of image Percentage (1-100) Region to capture Show save dialog Take another shot Thumbnail for webpages Window Your screenshot has been saved. antiXscreenshot Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-15 17:00+0200
Last-Translator: Pierluigi Mario <pierluigimariomail@gmail.com>
Language-Team: Italian (http://www.transifex.com/anticapitalista/antix-development/language/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.2.1
 Scegli una directory Crea un'anteprima Area personalizzata Ritardo in secondi L'estensione del file è stata cambiata.\nPer favore, riprova. Tipo di file Schermo intero Salvare questa schermata? Schermate multiple Nome dell'immagine Percentuale (1-100) Area da catturare Mostra le opzioni di salvataggio Cattura un altra schermata Anteprima per le pagine web Finestra La tua schermata è stata salvata. antiXcattura-schermo 